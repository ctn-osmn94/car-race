import { forwardRef } from 'react'
import { useGLTF } from '@react-three/drei'

import { useStore } from '../../store'

import type { CylinderProps } from '@react-three/cannon'
import type { Group, Mesh, MeshStandardMaterial } from 'three'
import type { GLTF } from 'three-stdlib'
import { useCylinder } from '@react-three/cannon'

interface WheelGLTF extends GLTF {
  nodes: {
    wheel_1: Mesh
    wheel_2: Mesh
    wheel_3: Mesh
  }
  materials: {
    Rubber: MeshStandardMaterial
    Steel: MeshStandardMaterial
    Chrom: MeshStandardMaterial
  }
}

interface WheelProps extends CylinderProps {
  leftSide?: boolean
}

export const Wheel = forwardRef<Group, WheelProps>(({ leftSide, ...props }, ref) => {
  const { radius } = useStore((state) => state.wheelInfo)
  const { nodes, materials } = useGLTF('/models/wheel.glb') as WheelGLTF
  const scale = radius / 0.3
  useCylinder(() => ({ mass: 1, type: 'Kinematic', material: 'wheel', collisionFilterGroup: 0, args: [radius, radius, 0.5, 16], ...props }), ref)
  return (
    <group ref={ref} dispose={null}>
      <group scale={scale}>
        <group rotation={[0, 0, ((leftSide ? 1 : -1) * Math.PI) / 2]}>
          <mesh castShadow geometry={nodes.wheel_1.geometry} material={materials.Rubber} />
          <mesh castShadow geometry={nodes.wheel_2.geometry} material={materials.Steel} />
          <mesh castShadow geometry={nodes.wheel_3.geometry} material={materials.Chrom} />
        </group>
      </group>
    </group>
  )
})
